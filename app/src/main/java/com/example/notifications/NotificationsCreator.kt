package com.example.notifications

import android.content.Context
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.example.krasnonosov_l17_t1.R

const val CHANNEL_ID = "channelId"

class NotificationsCreator(private val context: Context)   {

    fun createNotification(id: Int, title: String, content: String) {

        val builder = NotificationCompat.Builder(context, CHANNEL_ID).apply {
            setSmallIcon(R.drawable.ic_whatshot_red_40p)
            setContentTitle(title)
            setContentText(content)
            priority = NotificationCompat.PRIORITY_DEFAULT
            setAutoCancel(true)
        }

        with (NotificationManagerCompat.from(context)) {
            notify(id, builder.build())
        }
    }

}