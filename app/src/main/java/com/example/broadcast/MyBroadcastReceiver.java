package com.example.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.widget.TextView;

import com.example.krasnonosov_l17_t1.R;
import com.example.notifications.NotificationsCreator;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MyBroadcastReceiver extends BroadcastReceiver implements Serializable {

    private SimpleDateFormat dateFormat;
    private TextView textView;
    private NotificationsCreator notificationsCreator;

    public MyBroadcastReceiver(TextView textView) {
        this.dateFormat = new SimpleDateFormat("HH:mm");
        this.textView = textView;
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        Date date = new Date();
        this.notificationsCreator = new NotificationsCreator(context);

        switch (intent.getAction()) {
            case Intent.ACTION_POWER_CONNECTED:
                textView.append(dateFormat.format(date) + "\t\t\t" + context.getResources().getString(R.string.charging) + "\n\n");
                notificationsCreator.createNotification(2, context.getResources().getString(R.string.charge_status),
                        context.getResources().getString(R.string.charging));
                break;
            case Intent.ACTION_POWER_DISCONNECTED:
                textView.append(dateFormat.format(date) + "\t\t\t" + context.getResources().getString(R.string.discharging) + "\n\n");
                notificationsCreator.createNotification(3, context.getResources().getString(R.string.charge_status),
                        context.getResources().getString(R.string.discharging));
                break;

        }

        int wifiState = intent.getIntExtra(WifiManager.EXTRA_WIFI_STATE, WifiManager.WIFI_STATE_UNKNOWN);
        switch (wifiState) {
            case WifiManager.WIFI_STATE_ENABLING:
                textView.append(dateFormat.format(date) + "\t\t\t"+context.getResources().getString(R.string.wifi_enabled)+"\n\n");
                notificationsCreator.createNotification(4, context.getResources().getString(R.string.wifi_status),
                        context.getResources().getString(R.string.wifi_enabled));
                break;
            case WifiManager.WIFI_STATE_DISABLING:
                textView.append(dateFormat.format(date) + "\t\t\t"+context.getResources().getString(R.string.wifi_disabled)+"\n\n");
                notificationsCreator.createNotification(5, context.getResources().getString(R.string.wifi_status),
                        context.getResources().getString(R.string.wifi_disabled));
                break;
        }

        CallStateListener callStateListener = new CallStateListener(context, textView, dateFormat);
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        telephonyManager.listen(callStateListener, PhoneStateListener.LISTEN_CALL_STATE);

    }

}

