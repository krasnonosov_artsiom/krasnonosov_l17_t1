package com.example.broadcast

import android.content.Context
import android.telephony.PhoneStateListener
import android.telephony.TelephonyManager
import android.widget.TextView
import com.example.krasnonosov_l17_t1.R
import com.example.notifications.NotificationsCreator
import java.text.SimpleDateFormat
import java.util.*

class CallStateListener(var context: Context, var textView: TextView, var simpleDateFormat: SimpleDateFormat) :
    PhoneStateListener() {

    override fun onCallStateChanged(state: Int, phoneNumber: String?) {
        super.onCallStateChanged(state, phoneNumber)
        val notificationsCreator = NotificationsCreator(context)
        when (state) {
            TelephonyManager.CALL_STATE_RINGING -> {
                textView.append(simpleDateFormat.format(Date()) + "\t\t\t$phoneNumber phoned you.\n\n")
                notificationsCreator.createNotification(1, context.resources.getString(R.string.phone_status), "$phoneNumber phoned you.")
            }
        }
    }
}